/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.common;

public interface Constants {

    public static final int BACKSPACE = '\b';
    public static final int TAB = '\t';
    public static final int FORM_FEED = '\f';
    public static final int NEW_LINE = '\n';
    public static final int CARRIAGE_RETURN = '\r';
    public static final int SPACE = ' ';
    public static final int ESCAPE = '\\';
    public static final int QUOTATION_MARK = '\"';
    public static final int OPEN_ARRAY = '[';
    public static final int OPEN_OBJECT = '{';
    public static final int CLOSE_ARRAY = ']';
    public static final int CLOSE_OBJECT = '}';
    public static final int VALUE_SEPARATOR = ',';
    public static final int NAME_SEPARATOR = ':';
    public static final char[] DIGITS =
            new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
    public static final int DECIMAL_POINT = '.';
    public static final int MINUS = '-';
    public static final int PLUS = '+';
    public static final char[] EXPS = new char[]{'e', 'E'};
    public static final int QUOTATION = '"';
    public static final int REVERSE_SOLIDUS = '\\';
    public static final int SOLIDUS_CHAR = '/';
    public static final int BACKSPACE_CHAR = 'b';
    public static final int FORM_FEED_CHAR = 'f';
    public static final int NEW_LINE_CHAR = 'n';
    public static final int CARRIAGE_RETURN_CHAR = 'r';
    public static final int TAB_CHAR = 't';
    public static final int HEX_CHAR = 'u';
    public static final char[] HEXDIGITS =
            new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'A', 'B', 'C', 'D', 'E', 'F'};
    public static final String TRUE_STR = "true";
    public static final String FALSE_STR = "false";
    public static final String NULL_STR = "null";
    public static final int LEFT_SQUARE_BRACKET = '[';
    public static final int RIGHT_SQUARE_BRACKET = ']';
    public static final int LEFT_PARENTHESIS = '(';
    public static final int RIGHT_PARENTHESIS = ')';
    public static final int ALL_CHAR = '*';
    public static final int LAST_CHAR = '$';
    public static final int EXPRESSION_CHAR = '?';
    public static final int PERIOD_CHAR = '.';
    public static final int CURRENT_ELEMENT_CHAR = '@';
    public static final int COMMA_CHAR = ',';
    public static final int COLON_CHAR = ':';
    public static final int LESS = '<';
    public static final int GREATER = '>';
    public static final int EQUAL = '=';
    public static final int AND = '&';
    public static final int OR = '|';
    public static final String LAST_PREDICATE = "last()";
    public static final String POSITION_PREDICATE = "position()";
}
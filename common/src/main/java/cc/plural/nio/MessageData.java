/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.nio;

import java.nio.channels.SocketChannel;

public class MessageData<T> {

    public T originator;
    public SocketChannel socket;
    public byte[] data;

    public MessageData(SocketChannel socket, byte[] data) {
        this.socket = socket;
        this.data = data;
    }

    public void setOriginator(T originator) {
        this.originator = originator;
    }

    public T getOriginator() {
        return this.originator;
    }

    /**
     * @return the socket
     */
    public SocketChannel getSocket() {
        return socket;
    }

    /**
     * @param socket the socket to set
     */
    public void setSocket(SocketChannel socket) {
        this.socket = socket;
    }

    /**
     * @return the data
     */
    public byte[] getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(byte[] data) {
        this.data = data;
    }
}

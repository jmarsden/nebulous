/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.nio;

import cc.plural.server.Server;
import cc.plural.server.ServerWorker;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.List;

public class QueuedWorker implements ServerWorker {

    protected final List<MessageData<Server>> workerQueue;

    public QueuedWorker() {
        workerQueue = new ArrayList<MessageData<Server>>();
    }

    public void process(SocketChannel socketChannel, byte[] data, int count, Server origin) {
        synchronized (workerQueue) {
            MessageData<Server> dataEvent = new MessageData<Server>(socketChannel, data);
            dataEvent.setOriginator(origin);
            workerQueue.add(dataEvent);
            workerQueue.notify();
        }
    }

    public boolean hasEvents() {
        synchronized (workerQueue) {
            boolean empty = workerQueue.isEmpty();
            return !empty;
        }
    }

    public MessageData<Server> removeFirstEvent() {
        synchronized (workerQueue) {
            MessageData<Server> event = workerQueue.remove(0);
            return event;
        }
    }
}

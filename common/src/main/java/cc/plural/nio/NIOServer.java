/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.nio;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.SelectorProvider;
import java.util.*;

import cc.plural.nio.ServerChangeEvent.TYPE;
import cc.plural.server.Server;
import cc.plural.server.ServerWorker;

public class NIOServer extends Server implements Runnable {

    private InetAddress address;
    private int port;
    private InetSocketAddress inetSocketAddress;
    private ServerSocketChannel serverSocketChannel;
    private Selector selector;
    private ServerWorker worker;
    private ByteBuffer readBuffer;
    private final Map<SocketChannel, List<ByteBuffer>> outboundData;
    private final List<ServerChangeEvent> serverEvents;

    public NIOServer(InetAddress hostAddress, int port, ServerWorker worker) throws IOException {
        this.address = hostAddress;
        this.port = port;
        this.worker = worker;

        readBuffer = ByteBuffer.allocate(8192);
        outboundData = new HashMap<SocketChannel, List<ByteBuffer>>();
        serverEvents = new ArrayList<ServerChangeEvent>();

        this.selector = SelectorProvider.provider().openSelector();
        this.serverSocketChannel = ServerSocketChannel.open();
        this.serverSocketChannel.configureBlocking(false);
        this.inetSocketAddress = new InetSocketAddress(this.address, this.port);
        this.serverSocketChannel.socket().bind(inetSocketAddress);
        this.serverSocketChannel.register(this.selector, SelectionKey.OP_ACCEPT);
    }

    public void run() {
        onOpen(inetSocketAddress);
        while (true) {
            try {
                synchronized (serverEvents) {
                    Iterator<ServerChangeEvent> events = serverEvents.iterator();
                    while (events.hasNext()) {
                        ServerChangeEvent event = events.next();
                        switch (event.getType()) {
                            case CHANGE_INTERESTED_OPS:
                                SocketChannel socket = event.getSocket();
                                SelectionKey key = socket.keyFor(this.selector);
                                if (key != null) {
                                    key.interestOps(event.getOps());
                                }
                        }
                    }
                    this.serverEvents.clear();
                }

                // Wait for an event one of the registered channels
                this.selector.select();

                // Iterate over the set of keys for which events are available
                Iterator<?> selectedKeys = this.selector.selectedKeys().iterator();
                while (selectedKeys.hasNext()) {
                    SelectionKey key = (SelectionKey) selectedKeys.next();
                    selectedKeys.remove();

                    if (!key.isValid()) {
                        continue;
                    }

                    // Check what event is available and deal with it
                    if (key.isAcceptable()) {
                        this.accept(key);
                    } else if (key.isReadable()) {
                        this.read(key);
                    } else if (key.isWritable()) {
                        this.write(key);
                    }
                }
            } catch (Exception ex) {

                break;
            }
        }
    }

    public void send(SocketChannel socketChannel, byte[] data) {
        synchronized (serverEvents) {
            // Indicate we want the interest ops set changed
            this.serverEvents.add(new ServerChangeEvent(socketChannel, TYPE.CHANGE_INTERESTED_OPS, SelectionKey.OP_WRITE));

            // And queue the data we want written
            synchronized (outboundData) {
                List<ByteBuffer> queue = outboundData.get(socketChannel);
                if (queue == null) {
                    queue = new ArrayList<ByteBuffer>();
                    outboundData.put(socketChannel, queue);
                }
                queue.add(ByteBuffer.wrap(data));
            }
        }
        onSend(socketChannel, data);
        this.selector.wakeup();
    }

    /*
     public void send(SocketChannel socketChannel, byte[] data, Worker<Server> responseHandler) {
     // TODO: This could be blocking?
     synchronized (serverEvents) {
     // Indicate we want the interest ops set changed
     this.serverEvents.add(new ServerChangeEvent(socketChannel, TYPE.CHANGE_INTERESTED_OPS, SelectionKey.OP_WRITE));

     // And queue the data we want written
     synchronized (outboundData) {
     List<ByteBuffer> queue = outboundData.get(socketChannel);
     if (queue == null) {
     queue = new ArrayList<ByteBuffer>();
     outboundData.put(socketChannel, queue);
     }
     queue.add(ByteBuffer.wrap(data));
     }
     }
     onSend(socketChannel, data);
     this.selector.wakeup();
     }
     */
    public void close(SocketChannel socketChannel) throws IOException {
        socketChannel.close();
        onClose(socketChannel);
    }

    private void accept(SelectionKey key) throws IOException {
        ServerSocketChannel serverSocketChannel = (ServerSocketChannel) key.channel();
        SocketChannel socketChannel = serverSocketChannel.accept();
        socketChannel.configureBlocking(false);
        socketChannel.register(this.selector, SelectionKey.OP_READ);
        onAccept(socketChannel);
    }

    private void read(SelectionKey key) throws IOException {
        SocketChannel socketChannel = (SocketChannel) key.channel();
        this.readBuffer.clear();
        int numRead;
        try {
            numRead = socketChannel.read(this.readBuffer);
        } catch (IOException e) {
            // The remote forcibly closed the connection, cancel
            // the selection key and close the channel.
            key.cancel();
            socketChannel.close();
            return;
        }

        if (numRead == -1) {
            // Remote entity shut the socket down cleanly. Do the
            // same from our end and cancel the channel.
            key.channel().close();
            key.cancel();
            return;
        }

        byte[] byteClone = new byte[numRead];
        readBuffer.rewind();
        readBuffer.get(byteClone);

        /*
         System.out.println("\r\n*****************\r\nSent Request\r\n*****************\r\n");
         System.out.println(new String(byteClone));
         System.out.println("\r\n*****************\r\nEnd Request\r\n*****************\r\n");
         */

        if (this.worker != null) {
            this.worker.process(socketChannel, byteClone, numRead, this);
        }
    }

    private void write(SelectionKey key) throws IOException {
        SocketChannel socketChannel = (SocketChannel) key.channel();
        synchronized (outboundData) {
            List<ByteBuffer> queue = outboundData.get(socketChannel);
            while (!queue.isEmpty()) {
                ByteBuffer byteBuffer = (ByteBuffer) queue.get(0);
                int count = socketChannel.write(byteBuffer);
                onWrite(socketChannel, count);
                if (byteBuffer.remaining() > 0) {
                    break;
                }
                queue.remove(0);
            }
            if (queue.isEmpty()) {
                key.interestOps(SelectionKey.OP_READ);
            }
        }
    }

    public InetAddress getAddress() {
        return address;
    }

    public int getPort() {
        return port;
    }

    public ServerWorker getWorker() {
        return worker;
    }

    public void setWorker(ServerWorker worker) {
        this.worker = worker;
    }

    public void onOpen(InetSocketAddress inetSocketAddress) {
    }

    public void onAccept(SocketChannel socketChannel) {
    }

    public void onRead(SocketChannel socketChannel, byte[] data, int count) {
    }

    public void onSend(SocketChannel socketChannel, byte[] data) {
    }

    public void onWrite(SocketChannel socketChannel, int count) {
    }

    public void onClose(SocketChannel socketChannel) {
    }
}

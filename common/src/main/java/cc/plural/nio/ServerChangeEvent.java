/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.nio;

import java.nio.channels.SocketChannel;

public class ServerChangeEvent {

    public enum TYPE {

        CHANGE_INTERESTED_OPS
    }
    private final SocketChannel socket;
    private final TYPE type;
    private final int ops;

    public ServerChangeEvent(SocketChannel socket, TYPE type, int ops) {
        if (socket == null) {
            throw new NullPointerException("Socket Cannot Be null.");
        }
        if (type == null) {
            throw new NullPointerException("Type Cannot Be null.");
        }
        this.socket = socket;
        this.type = type;
        this.ops = ops;
    }

    /**
     * @return the socket
     */
    public SocketChannel getSocket() {
        return socket;
    }

    /**
     * @return the type
     */
    public TYPE getType() {
        return type;
    }

    /**
     * @return the ops
     */
    public int getOps() {
        return ops;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ServerChangeEvent [" + socket + " type:\"" + type + "\", ops:" + ops + "]";
    }
}

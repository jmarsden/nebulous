/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.parser;

import java.io.IOException;

/**
 *
 * @author openecho
 */
public interface ReaderParser {

    /**
     * @return the position
     */
    public Position getPosition();

    /**
     * Only here for POJO reasons.
     * @return hasPeeked()
     * @see hasPeeked()
     */
    public boolean isHasPeeked();

    public boolean hasPeeked();

    public void setHasPeeked(boolean hasPeeked);

    public int getLineNumber();

    public int getPositionNumber();

    public int peek() throws IOException, ParserException;

    public int read() throws IOException, ParserException;

    public void close();
}

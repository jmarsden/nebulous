/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.parser;

import static cc.plural.common.Constants.*;

public class ReaderParserConstantUtility {

    public boolean isDigit(int r) {
        return r >= DIGITS[0] && r <= DIGITS[9];
    }

    public boolean isDigit(char c) {
        return isDigit((int) c);
    }

    public boolean isNumeric(int r) {
        if (r == PLUS) {
            // TODO: Make about invalid JSON
            return true;
        }
        return r == MINUS || isDigit(r);
    }

    public boolean isNumeric(char c) {
        return isNumeric((int) c);
    }

    public boolean isHexDigit(int r) {
        return ( r >= HEXDIGITS[0] && r <= HEXDIGITS[9] )
                || ( r >= HEXDIGITS[10] && r <= HEXDIGITS[15] )
                || ( r >= HEXDIGITS[16] && r <= HEXDIGITS[21] );
    }

    public boolean isHexDigit(char c) {
        return isHexDigit((int) c);
    }

    public boolean isWhiteSpace(int r) {
        return r == SPACE || r == TAB;
    }

    public boolean isWhiteSpace(char c) {
        return isWhiteSpace((int) c);
    }

    public boolean isNewLine(int r) {
        return r == CARRIAGE_RETURN || r == NEW_LINE;
    }

    public boolean isNewLine(char c) {
        return isNewLine((int) c);
    }

    public boolean isReturn(int r) {
        return r == CARRIAGE_RETURN;
    }

    public boolean isReturn(char c) {
        return isReturn((int) c);
    }

    public boolean isLineFeed(int r) {
        return r == NEW_LINE;
    }

    public boolean isLineFeed(char c) {
        return isLineFeed((int) c);
    }

    public boolean isValidStringChar(int r) {
        return //( r >= 93 && r <= 1114111 ) || ( r >= 35 && r <= 91 ) || ( r == 32 ) || ( r == 33 );
                r >= 32 && r != 34 && r != 92;
    }
}

/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.parser;

import java.lang.reflect.Array;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Internationised Parser Exception.
 *
 * @author J.W.Marsden
 * @version 1.0.0
 */
public abstract class ParserException extends BaseParserException {

    /**
     * Serial UID
     */
    private static final long serialVersionUID = 470719429874019122L;
    /**
     * Exception Key
     */
    protected String key;
    /**
     * Exception Locale
     */
    protected Locale locale;

    /**
     * Basic Constructor.
     *
     * @param key Exception key
     * @param args Additional Arguments for Exception
     */
    public ParserException(String key, Object... args) {
	this(key, -1, -1, null, args);
    }

    /**
     * Constructor Including Line Number and Position Number of Exception
     *
     * @param key Exception Key
     * @param line Exception Line
     * @param position Exception Position
     * @param args Additional Arguments for Exception
     */
    public ParserException(String key, int line, int position, Object... args) {
	this(key, line, position, null, args);
    }

    /**
     * Constructor Including Line Number, Position Number and Locale of Exception.
     *
     * @param key Exception Key
     * @param line Exception Line
     * @param position Exception Position
     * @param locale Valid Locale for the exception
     * @param args Additional Arguments for Exception
     */
    public ParserException(String key, int line, int position, Locale locale, Object... args) {
	this.line = line;
	this.position = position;
	this.key = key;

	/**
	 * TODO: Fix this. Dont force it to be English!
	 */
	this.locale = ((locale == null) ? Locale.ENGLISH : locale);
	if (this.locale != null) {
	    try {
		String messageFormat = ResourceBundle.getBundle(getBundleName()).getString(this.key);
		this.message = String.format(messageFormat, args);
	    } catch (Exception ex) {
		StringBuilder argumentStringBuilder = new StringBuilder();
		Object argValue;
		int argCount;
		if ((argCount = Array.getLength(args)) > 0) {
		    for (int i = 0; i < argCount - 1; i++) {
			argValue = args[i];
			if (argValue != null) {
			    argumentStringBuilder.append(args.toString()).append(',');
			} else {
			    argumentStringBuilder.append("null").append(',');
			}
		    }
		    argValue = args[argCount - 1];
		    if (argValue != null) {
			argumentStringBuilder.append(argValue.toString());
		    } else {
			argumentStringBuilder.append("null");
		    }
		}
		String messageFormat = "Message Format Not Found (%s#%s[%s]): %s";
		this.message = String.format(messageFormat, getBundleName(), this.key, argumentStringBuilder.toString(), ex);
	    }
	} else {
	    this.message = String.format("Undefined Exception %s %s", key, locale);
	}
    }

    public abstract String getBundleName();
}

/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.parser;

public class BaseParserException extends Exception {

    /**
     * Serial UID
     */
    private static final long serialVersionUID = -4518394051006246504L;
    protected int line;
    protected int position;
    protected String message;

    public BaseParserException() {
    }

    public BaseParserException(String message) {
        this(-1, -1, message);
    }

    public BaseParserException(int line, int position, String message) {
        this.line = line;
        this.position = position;
        this.message = message;
    }

    @Override
    public String getMessage() {
        String output = "Parsing Exception";
        if (line != -1 || position != -1) {
            output = String.format("%s (%s,%s): %s", output, line, position, message);
        } else {
            output = String.format("%s: %s", output, message);
        }
        return output;
    }
}
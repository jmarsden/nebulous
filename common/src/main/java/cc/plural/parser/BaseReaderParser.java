/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.parser;

import java.io.IOException;

/**
 *
 * @author openecho
 */
public abstract class BaseReaderParser implements ReaderParser {

    protected int peekValue;
    protected boolean hasPeeked;
    protected Position position;
    protected ReaderParserConstantUtility constants;

    public BaseReaderParser() {
	this(null);
    }

    public BaseReaderParser(ReaderParserConstantUtility constants) {
	this.peekValue = -1;
	this.hasPeeked = false;
	this.position = new Position();
	if (constants == null) {
	    this.constants = new ReaderParserConstantUtility();
	} else {
	    this.constants = constants;
	}
    }

    /**
     * @return the position
     */
    public synchronized Position getPosition() {
	return position;
    }

    /**
     * @param position the position to set
     */
    protected Position setPosition(Position position) {
	return this.position = position;
    }

    /**
     * Only here for POJO reasons.
     *
     * @return hasPeeked()
     * @see hasPeeked()
     */
    public synchronized boolean isHasPeeked() {
	return hasPeeked();
    }

    public synchronized boolean hasPeeked() {
	return hasPeeked;
    }

    public synchronized void setHasPeeked(boolean hasPeeked) {
	this.hasPeeked = hasPeeked;
    }

    public int getLineNumber() {
	return getPosition().getLineNumber();
    }

    public int getPositionNumber() {
	return getPosition().getPostionNumber();
    }

    public synchronized int peek() throws IOException, ParserException {
	if (!hasPeeked) {
	    peekValue = readNext();
	    hasPeeked = true;
	}
	return peekValue;
    }

    public synchronized int read() throws IOException, ParserException {
	if (hasPeeked) {
	    hasPeeked = false;
	    return peekValue;
	}
	return readNext();
    }

    /**
     * Reads from the reader.
     *
     * @param targetReader The reader to be read from.
     * @return The read byte if found otherwise -1 if the end of the stream is reached.
     * @throws IOException Java IO Exception.
     */
    protected abstract int readNext() throws IOException, ParserException;

    public void close() {
    }

    protected void handleNewLine() throws IOException {
	getPosition().newLine();
    }

    @Override
    public String toString() {
	String state = "";
	try {
	    state = String.format("Next Char %s", (char) peek());
	} catch (IOException e) {
	    state = String.format("Unknown State: %s", e.toString());
	} catch (BaseParserException e) {
	    state = String.format("Unknown State: %s", e.toString());
	}
	return String.format("Reader %s: %s", getPosition(), state);
    }
}

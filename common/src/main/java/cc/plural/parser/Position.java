/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.parser;

public class Position {

    protected int lineNumber;
    protected int postionNumber;

    public Position() {
        this.lineNumber = 1;
        this.postionNumber = 0;
    }

    public int getLineNumber() {
        return lineNumber;
    }

    public int setLineNumber(int lineNumber) {
        return this.lineNumber = lineNumber;
    }

    public int newLine() {
        postionNumber = 0;
        return lineNumber++;
    }

    public int getPostionNumber() {
        return postionNumber;
    }

    public int setPostionNumber(int postionNumber) {
        return this.postionNumber = postionNumber;
    }

    final public int movePosition() {
        return postionNumber++;
    }

    @Override
    public String toString() {
        return String.format("(%s,%s)", getLineNumber(), getPostionNumber());
    }
}

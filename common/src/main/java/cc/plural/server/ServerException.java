/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.server;

import java.util.Locale;

import cc.plural.parser.ParserException;

public class ServerException extends ParserException {

    /**
     * Serial UID
     */
    private static final long serialVersionUID = -6405413556355975683L;

    public ServerException(String key, int line, int position, Locale locale, Object... args) {
        super(key, line, position, locale, args);
    }

    public ServerException(String key, int line, int position, Object... args) {
        super(key, line, position, args);
    }

    public ServerException(String key, Object... args) {
        super(key, args);
    }

    @Override
    public String getBundleName() {
        return "ServerMessages";
    }
}

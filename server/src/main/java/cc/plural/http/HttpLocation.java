/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.http;

import cc.plural.http.renderer.Renderer;

public class HttpLocation {

    private String[] hostPatterns;
    private int[] ports;
    private Renderer renderer;
    private String accessLog;
    private String errorLog;

    public HttpLocation() {
	hostPatterns = null;
	ports = null;
	renderer = null;
	accessLog = null;
	errorLog = null;
    }

    public String[] getHostPatterns() {
	return hostPatterns;
    }

    public void setHostPatterns(String[] hostPatterns) {
	this.hostPatterns = hostPatterns;
    }

    public int[] getPorts() {
	return ports;
    }

    public void setPorts(int[] ports) {
	this.ports = ports;
    }

    public Renderer getRenderer() {
	return renderer;
    }

    public void setRenderer(Renderer renderer) {
	this.renderer = renderer;
    }

    public String getAccessLog() {
	return accessLog;
    }

    public void setAccessLog(String accessLog) {
	this.accessLog = accessLog;
    }

    public String getErrorLog() {
	return errorLog;
    }

    public void setErrorLog(String errorLog) {
	this.errorLog = errorLog;
    }
    
    public boolean matchesPort(int port) {
	if(ports == null) {
	    return false;
	}
	if(ports.length == 1) {
	    return ports[0] == port;
	} else {
	    for(int i=0;i<ports.length;i++) {
		if(ports[i] == port) {
		    return true;
		}
	    }
	    return false;
	}
    }
    
    public boolean matchesHost(String host) {
	return true;
    }
}

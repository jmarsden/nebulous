/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.http;

import cc.plural.common.Constants;

public interface HttpConstants extends Constants {

    public static final String GET_STR = "GET";
    public static final String PUT_STR = "PUT";
    public static final String POST_STR = "POST";
    public static final String DELETE_STR = "DELETE";
    public static final String HEAD_STR = "HEAD";
    public static final String HTTP_1_0_STR = "HTTP/1.0";
    public static final String HTTP_1_1_STR = "HTTP/1.1";
    public static final String HTTP_MONDAY = "Mon";
    public static final String HTTP_TUESDAY = "Tue";
    public static final String HTTP_WEDNESDAY = "Wed";
    public static final String HTTP_THURSDAY = "Thu";
    public static final String HTTP_FRIDAY = "Fri";
    public static final String HTTP_SATURDAY = "Sat";
    public static final String HTTP_SUNDAY = "Sun";
    public static final String HTTP_JANUARY = "Jan";
    public static final String HTTP_FEBRUARY = "Feb";
    public static final String HTTP_MARCH = "Mar";
    public static final String HTTP_APRIL = "Apr";
    public static final String HTTP_MAY = "May";
    public static final String HTTP_JUNE = "Jun";
    public static final String HTTP_JULY = "Jul";
    public static final String HTTP_AUGUST = "Aug";
    public static final String HTTP_SEPTEMBER = "Sep";
    public static final String HTTP_OCTOBER = "Oct";
    public static final String HTTP_NOVEMBER = "Nov";
    public static final String HTTP_DECEMBER = "Dec";
}

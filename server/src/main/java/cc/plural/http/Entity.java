/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.http;

import java.lang.reflect.Array;

public class Entity {

    private byte[] entityMessage;

    public Entity() {
        entityMessage = null;
    }

    public boolean hasMessageData() {
        return entityMessage != null && Array.getLength(entityMessage) != 0;
    }

    public int messageDataSize() {
        if (entityMessage == null) {
            return 0;
        } else {
            return Array.getLength(entityMessage);
        }
    }

    public byte[] getMessageData() {
        return entityMessage;
    }

    public void setMessageData(byte[] data) {
        this.entityMessage = data;
    }
}

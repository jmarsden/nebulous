/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.http;

import java.util.Locale;

import cc.plural.parser.ParserException;

public class HttpParseException extends ParserException {

    /**
     *
     */
    private static final long serialVersionUID = 1041930346600970896L;

    /**
     * @param key
     * @param line
     * @param position
     * @param locale
     * @param args
     */
    public HttpParseException(String key, int line, int position, Locale locale, Object... args) {
	super(key, line, position, locale, args);
    }

    /**
     * @param key
     * @param line
     * @param position
     * @param args
     */
    public HttpParseException(String key, int line, int position, Object... args) {
	super(key, line, position, args);
    }

    @Override
    public String getBundleName() {
	return "HTTPMessages";
    }
}

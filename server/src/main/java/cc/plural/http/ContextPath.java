/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.http;

public class ContextPath {

    private String contextPath;

    public ContextPath(String contextPath) {
	this.contextPath = contextPath;
    }

    public String getContextPath() {
	return contextPath;
    }

    public boolean endsInSlash() {
	return contextPath.charAt(contextPath.length() - 1) == '/' || contextPath.charAt(contextPath.length() - 1) == '\\';
    }

    public String getExtension() {
	if (contextPath == null) {
	    return null;
	}
	String extension = null;
	if (contextPath.indexOf('.') > -1) {
	    int lastIndex = contextPath.lastIndexOf('.');
	    int length = contextPath.length();
	    if (lastIndex + 1 < length) {
		extension = contextPath.substring(lastIndex + 1, length);
	    }
	}
	return extension;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result
		+ ((contextPath == null) ? 0 : contextPath.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj) {
	    return true;
	}
	if (obj == null) {
	    return false;
	}
	if (getClass() != obj.getClass()) {
	    return false;
	}
	ContextPath other = (ContextPath) obj;
	if (contextPath == null) {
	    if (other.contextPath != null) {
		return false;
	    }
	} else if (!contextPath.equals(other.contextPath)) {
	    return false;
	}
	return true;
    }

    @Override
    public String toString() {
	return contextPath;
    }
}

/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.http;

public class HttpException extends Exception {

    /**
     * Serial UID
     */
    private static final long serialVersionUID = -5367408440493030614L;

    /**
     *
     */
    public HttpException() {
	super();
    }

    /**
     * @param message
     * @param cause
     */
    public HttpException(String message, Throwable cause) {
	super(message, cause);
    }

    /**
     * @param message
     */
    public HttpException(String message) {
	super(message);
    }

    /**
     * @param cause
     */
    public HttpException(Throwable cause) {
	super(cause);
    }
}

/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.http;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;

import cc.plural.http.headers.ContentLengthHeader;
import cc.plural.http.headers.DateValue;
import cc.plural.http.headers.Header;
import cc.plural.http.headers.ServerHeader;

public class HttpResponse extends HttpMessage {

    private HttpRequest httpRequest;
    private HttpStatus status;

    protected HttpResponse() {
	this(HttpStatus.NOT_FOUND, HttpTransferProtocol.HTTP_1_1);
    }

    protected HttpResponse(HttpStatus status) {
	this(status, HttpTransferProtocol.HTTP_1_1);
    }

    protected HttpResponse(HttpStatus status, HttpTransferProtocol protocol) {
	this.status = status;
	this.headerMap = new HashMap<String, Header>();
	this.httpProtocol = protocol;
    }

    public HttpRequest getHttpRequest() {
	return httpRequest;
    }

    public void setHttpRequest(HttpRequest httpRequest) {
	this.httpRequest = httpRequest;
    }

    public HttpStatus getStatus() {
	return status;
    }

    public void setStatus(HttpStatus status) {
	this.status = status;
    }

    public byte[] getResponseData() {
	ByteArrayOutputStream outputStream = new ByteArrayOutputStream(1024);

	StringBuilder outputBuilder = new StringBuilder();
	outputBuilder.append(getHttpProtocol()).append(' ').append(status.getCode()).append(' ').append(status.getMessage()).append("\r\n");
	//outputBuilder.append("Content-Length: " + ((data != null) ? Array.getLength(data) : 0)).append("\r\n");

	Iterator<String> keyIterator = headerMap.keySet().iterator();
	while (keyIterator.hasNext()) {
	    String key = keyIterator.next();
	    Header header = headerMap.get(key);
	    outputBuilder.append(header).append("\r\n");
	}
	outputBuilder.append("\r\n");

	try {
	    outputStream.write(outputBuilder.toString().getBytes());

	    if (hasEntity() && getEntity() != null) {
		outputStream.write(getEntity().getMessageData());
	    }
	} catch (IOException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}

	byte[] byteData = outputStream.toByteArray();
	return byteData;
    }

    public String toString() {
	StringBuilder outputBuilder = new StringBuilder("HttpResponse [");
	//outputBuilder.append(requestUUID).append(' ');
	outputBuilder.append("httpProtocol:").append('\"').append(httpProtocol).append('\"').append(' ');
	outputBuilder.append("httpStatus:").append('\"').append(status).append('\"');

	outputBuilder.append(' ').append("headers:{");
	Iterator<String> keyIterator = headerMap.keySet().iterator();
	while (keyIterator.hasNext()) {
	    String key = keyIterator.next();
	    outputBuilder.append(headerMap.get(key));
	    if (keyIterator.hasNext()) {
		outputBuilder.append(' ');
	    }
	}

	outputBuilder.append("}");
	outputBuilder.append("]");
	return outputBuilder.toString();
    }

    public static HttpResponse createStandardHttpResponse() {
	HttpResponse response = new HttpResponse();
	response.addHeader(ServerHeader.create());
	response.addHeader(DateValue.create());
	response.addHeader(ContentLengthHeader.create(response));
	return response;
    }
}

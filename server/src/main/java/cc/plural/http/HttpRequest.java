/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.http;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.UUID;

import cc.plural.common.Constants;
import cc.plural.http.headers.*;
import cc.plural.parser.ParserException;

import static cc.plural.http.HttpConstants.*;

public class HttpRequest extends HttpMessage {

    private UUID requestUUID;
    private HttpMethod httpMethod;
    private ContextPath contextPath;

    public HttpRequest() {
        requestUUID = UUID.randomUUID();
    }

    public UUID getRequestUUID() {
        return requestUUID;
    }

    /**
     * @return the httpMethod
     */
    public HttpMethod getHttpMethod() {
        return httpMethod;
    }

    /**
     * @param httpMethod the httpMethod to set
     */
    public void setHttpMethod(HttpMethod httpMethod) {
        this.httpMethod = httpMethod;
    }

    /**
     * @return the contextPath
     */
    public ContextPath getContextPath() {
        return contextPath;
    }

    /**
     * @param contextPath the contextPath to set
     */
    public void setContextPath(ContextPath contextPath) {
        this.contextPath = contextPath;
    }

    private static HttpMethod parseMethod(HttpRequestParser target) throws ParserException, IOException {
        HttpMethod method = null;
        if (target.peek() == GET_STR.charAt(0)) {
            for (int i = 0; i < GET_STR.length(); i++) {
                if (target.peek() == GET_STR.charAt(i)) {
                    target.read();
                } else {
                    throw new HttpParseException("invalidMethod", target.getLineNumber(), target.getPositionNumber(), (char) target.peek());
                }
            }
            method = HttpMethod.GET_METHOD;
        } else if (target.peek() == PUT_STR.charAt(0)) {
            if (target.peek() == PUT_STR.charAt(0)) {
                for (int i = 0; i < PUT_STR.length(); i++) {
                    if (target.peek() == PUT_STR.charAt(i)) {
                        target.read();
                    } else {
                        throw new HttpParseException("invalidMethod", target.getLineNumber(), target.getPositionNumber(), (char) target.peek());
                    }
                }
                method = HttpMethod.PUT_METHOD;
            } else if (target.peek() == POST_STR.charAt(0)) {
                for (int i = 0; i < POST_STR.length(); i++) {
                    if (target.peek() == POST_STR.charAt(i)) {
                        target.read();
                    } else {
                        throw new HttpParseException("invalidMethod", target.getLineNumber(), target.getPositionNumber(), (char) target.peek());
                    }
                }
                method = HttpMethod.POST_METHOD;
            }
        } else if (target.peek() == DELETE_STR.charAt(0)) {
            for (int i = 0; i < DELETE_STR.length(); i++) {
                if (target.peek() == DELETE_STR.charAt(i)) {
                    target.read();
                } else {
                    throw new HttpParseException("invalidMethod", target.getLineNumber(), target.getPositionNumber(), (char) target.peek());
                }
            }
            method = HttpMethod.DELETE_METHOD;
        } else if (target.peek() == HEAD_STR.charAt(0)) {
            for (int i = 0; i < HEAD_STR.length(); i++) {
                if (target.peek() == HEAD_STR.charAt(i)) {
                    target.read();
                } else {
                    throw new HttpParseException("invalidMethod", target.getLineNumber(), target.getPositionNumber(), (char) target.peek());
                }
            }
            method = HttpMethod.HEAD_METHOD;
        }
        if (target.peek() == Constants.SPACE) {
            target.read();
        } else {
            throw new HttpParseException("malformedMethod", target.getLineNumber(), target.getPositionNumber(), (char) target.peek());
        }

        if (method == null) {
            throw new HttpParseException("unknownMethod", target.getLineNumber(), target.getPositionNumber(), (char) target.peek());
        }
        return method;
    }

    private static ContextPath parseContext(HttpRequestParser target) throws ParserException, IOException {
        if (target.peek() == Constants.SPACE) {
            throw new HttpParseException("malformedContext", target.getLineNumber(), target.getPositionNumber(), (char) target.peek());
        }
        StringBuilder contextBuilder = new StringBuilder();
        while (target.peek() != Constants.SPACE) {
            contextBuilder.append((char) target.read());
            if (target.peek() == -1) {
                throw new HttpParseException("unexpectedEOF", target.getLineNumber(), target.getPositionNumber(), (char) target.peek());
            }
        }
        if (target.peek() == Constants.SPACE) {
            target.read();
        } else {
            throw new HttpParseException("malformedContext", target.getLineNumber(), target.getPositionNumber(), (char) target.peek());
        }
        return new ContextPath(contextBuilder.toString());
    }

    private static HttpTransferProtocol parseHTTPTransferProtocol(HttpRequestParser target) throws IOException, ParserException {
        HttpTransferProtocol protocol = null;
        StringBuilder builder = new StringBuilder();
        while (target.peek() != Constants.NEW_LINE && target.peek() != Constants.CARRIAGE_RETURN) {
            builder.append((char) target.read());
            if (target.peek() == -1) {
                throw new HttpParseException("unexpectedEOF", target.getLineNumber(), target.getPositionNumber(), (char) target.peek());
            }
        }
        String headerProtocol = builder.toString();
        if (headerProtocol.equals(HTTP_1_0_STR)) {
            protocol = HttpTransferProtocol.HTTP_1_0;
        } else if (headerProtocol.equals(HTTP_1_1_STR)) {
            protocol = HttpTransferProtocol.HTTP_1_1;
        }
        if (protocol == null) {
            throw new HttpParseException("invalidProtocol", target.getLineNumber(), target.getPositionNumber(), (char) target.peek());
        }

        while (target.peek() == Constants.NEW_LINE || target.peek() == Constants.CARRIAGE_RETURN) {
            target.read();
            if (target.peek() == -1) {
                throw new HttpParseException("unexpectedEOF", target.getLineNumber(), target.getPositionNumber(), (char) target.peek());
            }
        }

        return protocol;
    }

    private static HashMap<String, Header> parseHTTPHeaders(HeaderFactory headerFactory, HttpRequestParser target) throws ParserException, IOException {
        HashMap<String, Header> headers = new HashMap<String, Header>();
        StringBuilder builder = new StringBuilder();
        while (target.peek() != -1) {

            builder = new StringBuilder();
            while (target.peek() != Constants.COLON_CHAR) {
                builder.append((char) target.read());
                if (target.peek() == -1) {
                    throw new HttpParseException("unexpectedEOF", target.getLineNumber(), target.getPositionNumber(), (char) target.peek());
                }
            }

            if (target.peek() == Constants.COLON_CHAR) {
                target.read();
            } else {
                throw new HttpParseException("noColon", target.getLineNumber(), target.getPositionNumber(), (char) target.peek());
            }

            String fieldName = builder.toString().trim();

            builder = new StringBuilder();

            while (target.peek() != Constants.NEW_LINE && target.peek() != Constants.CARRIAGE_RETURN) {
                builder.append((char) target.read());
                if (target.peek() == -1) {
                    throw new HttpParseException("unexpectedEOF", target.getLineNumber(), target.getPositionNumber(), (char) target.peek());
                }
            }

            String value = builder.toString().trim();


            try {
                headers.put(fieldName, headerFactory.parseHeaderInstance(fieldName, value));
            } catch (HttpException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }


            if (target.peek() == Constants.CARRIAGE_RETURN || target.peek() == Constants.NEW_LINE) {
                target.read();
                if (target.peek() == Constants.CARRIAGE_RETURN || target.peek() == Constants.NEW_LINE) {
                    target.read();
                    break;
                }
            } else {
                throw new HttpParseException("malformedNewLine", target.getLineNumber(), target.getPositionNumber(), (char) target.peek());
            }
        }
        return headers;
    }

    public String toString() {
        StringBuilder outputBuilder = new StringBuilder("HttpRequest [");
        outputBuilder.append(requestUUID).append(' ');
        outputBuilder.append("httpMethod:").append('\"').append(httpMethod).append('\"').append(' ');
        outputBuilder.append("httpProtocol:").append('\"').append(getHttpProtocol()).append('\"').append(' ');
        // outputBuilder.append("host:").append('\"').append(getHost()).append('\"').append(' ');
        outputBuilder.append("contextPath:").append('\"').append(contextPath).append('\"').append(' ');

        outputBuilder.append("headers:{");
        java.util.Iterator<String> keyIterator = headerMap.keySet().iterator();
        while (keyIterator.hasNext()) {
            String key = keyIterator.next();
            outputBuilder.append(headerMap.get(key));
            if (keyIterator.hasNext()) {
                outputBuilder.append(' ');
            }
        }
        outputBuilder.append("}");

        outputBuilder.append("]");
        return outputBuilder.toString();
    }

    public static HttpRequest parseRequestData(HeaderFactory headerFactory, byte[] data) throws ParserException, IOException {
        ByteArrayInputStream inputStream = new ByteArrayInputStream(data);
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

        HttpRequest request = new HttpRequest();
        HttpRequestParser target = new HttpRequestParser(inputStreamReader);
        request.setHttpMethod(parseMethod(target));
        request.setContextPath(parseContext(target));
        request.setHttpProtocol(parseHTTPTransferProtocol(target));

        request.headerMap = parseHTTPHeaders(headerFactory, target);

        return request;
    }

    public HttpResponse createReponse() {
        HttpResponse response = new HttpResponse(HttpStatus.SERVER_ERROR, getHttpProtocol());
        response.setHttpRequest(this);
        response.addHeader(ServerHeader.create());
        response.addHeader(DateValue.create());
        response.addHeader(ContentLengthHeader.create(response));
        return response;
    }
}

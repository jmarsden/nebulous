/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.http;

import cc.plural.parser.BaseReaderParser;
import java.io.IOException;
import java.io.Reader;

public class HttpRequestParser extends BaseReaderParser {

    protected Reader reader;

    public HttpRequestParser(Reader reader) {
        if (reader == null) {
            throw new NullPointerException("Null Reader Exception.");
        }
        this.reader = reader;
    }

    public Reader getReader() {
        return reader;
    }

    public void setReader(Reader reader) {
        this.reader = reader;
    }

    /**
     * Reads from the reader.
     *
     * @param targetReader The reader to be read from.
     * @return The read byte if found otherwise -1 if the end of the stream is
     * reached.
     * @throws IOException Java IO Exception.
     */
    @Override
    public int readNext() throws IOException {
        int r = -1;
        if (reader.ready() && (r = reader.read()) != -1) {
            getPosition().movePosition();
            if (r == '\r') {
                if (reader.ready() && (r = reader.read()) != -1) {
                    getPosition().movePosition();
                    if (r == '\n') {
                        r = '\r';
                    }
                }
                handleNewLine();
            }
        }
        if (r == 0) {
            r = -1;
        }
        return r;
    }
}

/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.http.headers;

import cc.plural.http.HttpResponse;


public class ContentLengthHeader extends Header {

	private HttpResponse response;

	protected ContentLengthHeader(String fieldName, HttpResponse response) {
		super(fieldName);
		this.response = response;
	}
	
	/**
	 * @return the value
	 */
	public final String getValue() {
		if(response == null || !response.hasEntity()) {
			return "0";
		} else {
			return "" + response.getEntity().messageDataSize();
		}
	}

	public static Header create(HttpResponse response) {
		return new ContentLengthHeader("Content-Length", response);
	}
	
	public static Header create(String fieldName, HttpResponse response) {
		return new ContentLengthHeader(fieldName, response);
	}

	@Override
	public void setValue(String value) {
		// TODO Auto-generated method stub
		
	}
}

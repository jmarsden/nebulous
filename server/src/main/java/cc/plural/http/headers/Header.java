/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.http.headers;

public abstract class Header {

    public enum Type {

	General, Request, Response, Entity
    }
    private String fieldName;
    private Type type;

    public Header(String fieldName) {
	this.fieldName = fieldName;
	this.type = Type.Entity;
    }

    public Header(String fieldName, Type type) {
	this.fieldName = fieldName;
	this.type = type;
    }

    /**
     * @return the type
     */
    public Type getType() {
	return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(Type type) {
	this.type = type;
    }

    /**
     * @return the fieldName
     */
    public final String getFieldName() {
	return fieldName;
    }

    /**
     * @param fieldName the fieldName to set
     */
    public void setFieldName(String fieldName) {
	this.fieldName = fieldName;
    }

    /**
     * @return the value
     */
    public abstract String getValue();

    /**
     * @param fieldName the fieldName to set
     */
    public abstract void setValue(String value);

    public void parseValue(String value) {
	this.setValue(value);
    }

    public String toString() {
	return getFieldName() + ": " + getValue();
    }
}

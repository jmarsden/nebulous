/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.http.headers;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import cc.plural.http.HttpException;
import cc.plural.http.headers.Header.Type;
import cc.plural.jsonij.JSON;
import cc.plural.jsonij.JSON.Object;
import cc.plural.jsonij.Value;
import cc.plural.server.ServerException;

public class HeaderFactory {

    public static final String DEFAULT_HEADER_RESOURCE = "/etc/http/header.json";
    private Map<String, KnownHeader> knownHeaders;

    public HeaderFactory() {
        knownHeaders = new HashMap<String, KnownHeader>();
    }

    public boolean knownHeader(String name) {
        return knownHeaders.containsKey(name);
    }

    public Header createHeaderInstance(String name) throws HttpException {
        Class<?> headerClass = null;
        Type type = Type.Entity;
        if (knownHeaders.containsKey(name)) {
            KnownHeader knownHeader = knownHeaders.get(name);
            headerClass = knownHeader.getHeaderClass();
            type = knownHeader.getType();
        } else {
            headerClass = HeaderValue.class;
        }
        Header header;
        try {
            header = (Header) headerClass.newInstance();
        } catch (InstantiationException e) {
            throw new HttpException("httpHeaderCreate", e);
        } catch (IllegalAccessException e) {
            throw new HttpException("httpHeaderCreate", e);
        }
        header.setFieldName(name);
        header.setType(type);
        return header;
    }

    public Header createHeaderInstance(String name, String value) throws HttpException {
        Header header = createHeaderInstance(name);
        header.setValue(value);
        return header;
    }

    public Header parseHeaderInstance(String name, String value) throws HttpException {
        Class<?> headerClass = null;
        Type type = Type.Entity;
        if (knownHeaders.containsKey(name)) {
            KnownHeader knownHeader = knownHeaders.get(name);
            headerClass = knownHeader.getHeaderClass();
            type = knownHeader.getType();
        } else {
            headerClass = HeaderValue.class;
        }
        Header header;
        try {
            header = (Header) headerClass.newInstance();
        } catch (InstantiationException e) {
            throw new HttpException("httpHeaderCreate", e);
        } catch (IllegalAccessException e) {
            throw new HttpException("httpHeaderCreate", e);
        }
        header.setFieldName(name);
        header.setType(type);
        header.parseValue(value);
        return header;
    }

    public static HeaderFactory loadHeaderTypes() throws ServerException {
        return loadHeaderTypes(DEFAULT_HEADER_RESOURCE);
    }

    public static HeaderFactory loadHeaderTypes(String headerResource) throws ServerException {
        HeaderFactory factory = new HeaderFactory();

        URL headerURL = HeaderFactory.class.getResource(headerResource);
        if (headerURL != null) {
            InputStream inputStream = null;
            InputStreamReader inputStreamReader = null;
            try {
                inputStream = headerURL.openStream();
                inputStreamReader = new InputStreamReader(inputStream);
                JSON headerJSON = JSON.parse(inputStreamReader);
                if (headerJSON.getRoot().type() == Value.TYPE.OBJECT) {
                    @SuppressWarnings("unchecked")
                    JSON.Object<String, Value> headerJSONObject = (Object<String, Value>) headerJSON.getRoot();
                    Iterator<String> keyIterator = headerJSONObject.keySet().iterator();
                    while (keyIterator.hasNext()) {
                        String headerName = keyIterator.next();
                        Type type = null;
                        Class<?> clazz = null;

                        @SuppressWarnings("unchecked")
                        JSON.Object<String, Value> value = (JSON.Object<String, Value>) headerJSONObject.get(headerName);
                        if (value.has("type")) {
                            String typeString = value.get("type").getString();
                            if (typeString.equals("General")) {
                                type = Type.General;
                            } else if (typeString.equals("Request")) {
                                type = Type.Request;
                            } else if (typeString.equals("Response")) {
                                type = Type.Response;
                            } else if (typeString.equals("Entity")) {
                                type = Type.Entity;
                            }
                        } else {
                            type = Type.Entity;
                        }
                        if (value.has("class")) {
                            try {
                                clazz = Class.forName(value.get("class").getString());
                            } catch (ClassNotFoundException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                                clazz = HeaderValue.class;
                            }
                        } else {
                            clazz = HeaderValue.class;
                        }

                        KnownHeader knownHeader = new KnownHeader(headerName, type, clazz);
                        factory.knownHeaders.put(headerName, knownHeader);
                    }
                }

            } catch (IOException e) {
                // throw new ServerException("mimeLoad", mimeResource,
                // e.toString());
            } catch (cc.plural.jsonij.parser.ParserException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } finally {
                if (inputStreamReader != null) {
                    try {
                        inputStreamReader.close();
                    } catch (IOException e) {
                    }
                }
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e) {
                    }
                }
            }
        }
        return factory;
    }

    public static class KnownHeader {

        private String headerName;
        private Type type;
        private Class<?> headerClass;

        public KnownHeader() {
            this.headerName = null;
            this.type = null;
            this.headerClass = null;
        }

        public KnownHeader(String headerName, Type type, Class<?> headerClass) {
            this.headerName = headerName;
            this.type = type;
            this.headerClass = headerClass;
        }

        /**
         * @return the headerName
         */
        public String getHeaderName() {
            return headerName;
        }

        /**
         * @param headerName the headerName to set
         */
        public void setHeaderName(String headerName) {
            this.headerName = headerName;
        }

        /**
         * @return the type
         */
        public Type getType() {
            return type;
        }

        /**
         * @param type the type to set
         */
        public void setType(Type type) {
            this.type = type;
        }

        /**
         * @return the headerClass
         */
        public Class<?> getHeaderClass() {
            return headerClass;
        }

        /**
         * @param headerClass the headerClass to set
         */
        public void setHeaderClass(Class<?> headerClass) {
            this.headerClass = headerClass;
        }

        /*
         * (non-Javadoc)
         * 
         * @see java.lang.Object#toString()
         */
        @Override
        public String toString() {
            return "KnownHeader [headerName=" + headerName + ", type=" + type + ", headerClass=" + headerClass + "]";
        }
    }
}

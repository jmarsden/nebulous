/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.http.headers;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import cc.plural.http.HttpConstants;

public class DateValue extends Header {

	private Date date;

	public DateValue() {
		super(null, Type.Entity);
	}
	
	public DateValue(String fieldName, Date date) {
		super(fieldName);
		this.date = date;
	}
	
	/**
	 * @return the value
	 */
	public final String getValue() {
		return DateValue.dateToHTTPHeader(date);
	}
	
	@Override
	public void setValue(String value) {
		// TODO Auto-generated method stub
		
	}
	
	public Date getDate() {
		return this.date;
	}
	
	public void setDate(Date date) {
		this.date = date;
	}

	public static Header create() {
		return new DateValue("Date", new Date());
	}
	
	public static Header create(String fieldName) {
		return new DateValue(fieldName, new Date());
	}
	
	public static Header create(Date date) {
		return new DateValue("Date", date);
	}
	
	public static Header create(String fieldName, Date date) {
		return new DateValue(fieldName, date);
	}
	
	public static String dateToHTTPHeader(Date date) {
		Calendar calendar = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
		calendar.setTime(date);
		
		int dayInWeek = calendar.get(Calendar.DAY_OF_WEEK);
		int dayInMonth = calendar.get(Calendar.DAY_OF_MONTH);
		int month = calendar.get(Calendar.MONTH);
		int year =  calendar.get(Calendar.YEAR);
		int hour24 =  calendar.get(Calendar.HOUR_OF_DAY);
		int minute =  calendar.get(Calendar.MINUTE);
		int second =  calendar.get(Calendar.SECOND);
		
		String timeZone = calendar.getTimeZone().getID();
		
		String dayInWeekString = null;
		switch(dayInWeek) {
			case Calendar.MONDAY:
				dayInWeekString = HttpConstants.HTTP_MONDAY;
				break;
			case Calendar.TUESDAY:
				dayInWeekString = HttpConstants.HTTP_TUESDAY;
				break;
			case Calendar.WEDNESDAY:
				dayInWeekString = HttpConstants.HTTP_WEDNESDAY;
				break;
			case Calendar.THURSDAY:
				dayInWeekString = HttpConstants.HTTP_THURSDAY;
				break;
			case Calendar.FRIDAY:
				dayInWeekString = HttpConstants.HTTP_FRIDAY;
				break;
			case Calendar.SATURDAY:
				dayInWeekString = HttpConstants.HTTP_SATURDAY;
				break;
			case Calendar.SUNDAY:
				dayInWeekString = HttpConstants.HTTP_SUNDAY;
				break;
		}
		
		String dayInMonthString = null;
		if(dayInMonth < 10) {
			dayInMonthString = "0" + dayInMonth;
		} else {
			dayInMonthString = "" + dayInMonth;
		}
		
		String monthString = null;
		switch(month) {
			case Calendar.JANUARY:
				monthString = HttpConstants.HTTP_JANUARY;
				break;
			case Calendar.FEBRUARY:
				monthString = HttpConstants.HTTP_FEBRUARY;
				break;
			case Calendar.MARCH:
				monthString = HttpConstants.HTTP_MARCH;
				break;
			case Calendar.APRIL:
				monthString = HttpConstants.HTTP_APRIL;
				break;
			case Calendar.MAY:
				monthString = HttpConstants.HTTP_MAY;
				break;
			case Calendar.JUNE:
				monthString = HttpConstants.HTTP_JUNE;
				break;
			case Calendar.JULY:
				monthString = HttpConstants.HTTP_JULY;
				break;
			case Calendar.AUGUST:
				monthString = HttpConstants.HTTP_AUGUST;
				break;
			case Calendar.SEPTEMBER:
				monthString = HttpConstants.HTTP_SEPTEMBER;
				break;
			case Calendar.OCTOBER:
				monthString = HttpConstants.HTTP_OCTOBER;
				break;
			case Calendar.NOVEMBER:
				monthString = HttpConstants.HTTP_NOVEMBER;
				break;
			case Calendar.DECEMBER:
				monthString = HttpConstants.HTTP_DECEMBER;
				break;
		}
		
		String hour24String = null;
		if(hour24 < 10) {
			hour24String = "0" + hour24;
		} else {
			hour24String = "" + hour24;
		}
		
		String minuteString = null;
		if(minute < 10) {
			minuteString = "0" + minute;
		} else {
			minuteString = "" + minute;
		}
		
		String secondString = null;
		if(second < 10) {
			secondString = "0" + second;
		} else {
			secondString = "" + second;
		}
		
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(dayInWeekString).append(',').append(' ');
		stringBuilder.append(dayInMonthString).append(' ');
		stringBuilder.append(monthString).append(' ');
		stringBuilder.append(year).append(' ');
		stringBuilder.append(hour24String).append(':');
		stringBuilder.append(minuteString).append(':');
		stringBuilder.append(secondString).append(' ');
		stringBuilder.append(timeZone);
		return stringBuilder.toString();
	}
}

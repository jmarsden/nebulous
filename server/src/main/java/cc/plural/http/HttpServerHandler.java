/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.http;

import cc.plural.application.Mime;
import cc.plural.application.configuration.HttpSettings;
import cc.plural.application.configuration.LocationSettings;
import cc.plural.application.configuration.ServerSettings;
import cc.plural.http.headers.DateValue;
import cc.plural.http.headers.Header;
import cc.plural.http.headers.HeaderFactory;
import cc.plural.http.headers.HostHeader;
import cc.plural.nio.MessageData;
import cc.plural.parser.ParserException;
import cc.plural.resource.ResourceUtils;
import cc.plural.server.Server;
import cc.plural.server.ServerException;
import cc.plural.server.ServerHandler;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class HttpServerHandler extends ServerHandler {

    HttpSettings httpConfiguration;
    Mime mimes;
    String defaultMime;
    String[] index;
    HeaderFactory headerFactory;
    List<HttpLocation> locationList;

    public HttpServerHandler(HttpSettings httpConfiguration) {
        this.httpConfiguration = httpConfiguration;
        mimes = null;
        defaultMime = null;
        index = null;
        headerFactory = null;
        locationList = new ArrayList<HttpLocation>();
    }

    @Override
    public void configure() throws ServerException {
        defaultMime = httpConfiguration.getDefaultMime();
        index = httpConfiguration.getIndex();

        if (httpConfiguration.getHeaders() != null) {
            headerFactory = HeaderFactory.loadHeaderTypes(httpConfiguration.getHeaders());
        } else {
            headerFactory = HeaderFactory.loadHeaderTypes();
        }

        if (httpConfiguration.getMime() != null) {
            mimes = Mime.loadMimeTypes(httpConfiguration.getMime());
        } else {
            mimes = Mime.loadMimeTypes();
        }

        if (httpConfiguration.getServer() != null) {
            for (ServerSettings serverConfiguration : httpConfiguration.getServer()) {
                LocationSettings[] locationsConfiguration = serverConfiguration.getLocation();
                //ports = serverConfiguration.getPort();
                for (LocationSettings locationConfiguration : locationsConfiguration) {
                    HttpLocation location = new HttpLocation();
                    location.setPorts(ports);
                    location.setHostPatterns(locationConfiguration.path);
//		    try {
//			Class<?> handlerClass = Class.forName(locationConfiguration.renderer.handler);
//			Renderer renderer = (Renderer) handlerClass.newInstance();
//			location.setRenderer(renderer);
//		    } catch (InstantiationException ex) {
//			Logger.getLogger(HttpServerHandler.class.getName()).log(Level.SEVERE, null, ex);
//		    } catch (IllegalAccessException ex) {
//			Logger.getLogger(HttpServerHandler.class.getName()).log(Level.SEVERE, null, ex);
//		    } catch (ClassNotFoundException ex) {
//			Logger.getLogger(HttpServerHandler.class.getName()).log(Level.SEVERE, null, ex);
//		    }
                }
            }
        }
    }

    @Override
    public void handleMessage(MessageData<Server> messageData) throws ServerException {
        HttpRequest request = null;
        HttpResponse response = null;

        try {
            request = HttpRequest.parseRequestData(headerFactory, messageData.data);
        } catch (ParserException ex) {
            Logger.getLogger(HttpServerHandler.class.getName()).log(Level.SEVERE, null, ex);
            throw new ServerException("RequestParse", ex);
        } catch (IOException ex) {
            Logger.getLogger(HttpServerHandler.class.getName()).log(Level.SEVERE, null, ex);
            throw new ServerException("RequestParse", ex);
        }
        System.out.println("\r\nRequest:\r\n" + request + "\r\n");

        response = request.createReponse();
        HostHeader host = null;
        if (request.hasHeader("Host")) {
            host = (HostHeader) request.getHeader("Host");
        }

        ContextPath contextPath = request.getContextPath();

        for (HttpLocation location : locationList) {
        }

        if (request.hasHeader("Host")) {
            HostHeader hostHeader = (HostHeader) request.getHeader("Host");

            System.out.println(hostHeader + "" + contextPath);

            try {
                if (attemptDistributedResourceLoad(contextPath, response)) {
                    System.out.println("Serving Standard Resource for " + contextPath);
                } else {
                    response.setStatus(HttpStatus.NOT_FOUND);
                    URL resourceURL = getClass().getResource("/www/error.html");
                    byte[] data;
                    try {
                        data = ResourceUtils.obtainByteData(resourceURL);
                        response.setEntity(data);
                    } catch (IOException ex) {
                        Logger.getLogger(HttpServerHandler.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            } catch (IOException e) { // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (HttpException e) {
            }


        } else {
            response.setStatus(HttpStatus.BAD_REQUEST);
        }





        Server server = messageData.getOriginator();
        byte[] responseData = response.getResponseData();

        server.send(messageData.socket, responseData);
    }

    private boolean attemptDistributedResourceLoad(ContextPath contextPath, HttpResponse response) throws IOException, HttpException {
        String loadPath = "";

        if (contextPath != null && contextPath.endsInSlash()) {
            loadPath = "/www" + contextPath.toString() + httpConfiguration.getIndex()[0];
        } else {
            loadPath = "/www" + contextPath.toString();
        }

        URL resourceURL = getClass().getResource(loadPath);
        if (resourceURL != null) {
            URLConnection resourceConnection = resourceURL.openConnection();
            if (resourceConnection != null && resourceConnection.getContentLength() > 0) {
                String contentType = Mime.disambiguate(resourceConnection.getContentType(), mimes.getMimeForExtension(contextPath.getExtension()));

                Date lastModifiedDate = new Date(resourceConnection.getLastModified());

                DateValue lastModifiedHeader = (DateValue) headerFactory.createHeaderInstance("Last-Modified");
                lastModifiedHeader.setDate(lastModifiedDate);
                Header contentTypeHeader = headerFactory.createHeaderInstance("Content-Type", contentType);

                response.addHeader(lastModifiedHeader);
                response.addHeader(contentTypeHeader);
                response.setEntity(ResourceUtils.obtainByteData(resourceURL));
                response.setStatus(HttpStatus.OK);
                return true;
            }
        }
        return false;
    }
}

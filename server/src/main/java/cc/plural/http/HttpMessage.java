/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.http;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import cc.plural.http.headers.Header;

public abstract class HttpMessage {

	protected HttpTransferProtocol httpProtocol;
	
	protected Entity entity;
	
	protected Map<String, Header> headerMap;
	
	public HttpMessage() {
		this.httpProtocol = null;
		this.entity = null;
		this.headerMap = new HashMap<String, Header>();
	}
	
	public HttpTransferProtocol getHttpProtocol() {
		return httpProtocol;
	}

	public void setHttpProtocol(HttpTransferProtocol httpProtocol) {
		this.httpProtocol = httpProtocol;
	}
	
	public HttpTransferProtocol getHttpTransferProtocol() {
		return httpProtocol;
	}
	
	public boolean hasEntity() {
		return entity != null;
	}
	
	public Entity getEntity() {
		return entity;
	}
	
	public HttpMessage setEntity(Entity entity) {
		this.entity = entity;
		return this;
	}
	
	public Entity setEntity(byte[] data) {
		if(this.entity == null) {
			this.entity = new Entity();
		}
		this.entity.setMessageData(data);
		return this.entity;
	}
	
	public Set<String> getHeaderNames() {
		return headerMap.keySet();
	}

	public boolean hasHeader(String key) {
		return headerMap.containsKey(key);
	}

	public Header getHeader(String key) {
		return headerMap.get(key);
	}

	public Header deleteHeader(String key) {
		return headerMap.remove(key);
	}

	public Header addHeader(Header header) {
		headerMap.put(header.getFieldName(), header);
		return header;
	}
	
}

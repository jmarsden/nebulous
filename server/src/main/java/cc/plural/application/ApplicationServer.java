/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.application;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.channels.SocketChannel;
import java.util.logging.Level;
import java.util.logging.Logger;

import cc.plural.application.configuration.ApplicationSettings;
import cc.plural.nio.NIOServer;
import cc.plural.server.ServerWorker;
import cc.plural.websocket.WebSocketState;

public class ApplicationServer extends NIOServer {

    protected ApplicationSettings configuration;
    private int state;

    public ApplicationServer(InetAddress hostAddress, int port, ServerWorker worker) throws IOException {
        super(hostAddress, port, worker);
        state = WebSocketState.CONNECTING;
    }

    /**
     * @return the configuration
     */
    public ApplicationSettings getConfiguration() {
        return configuration;
    }

    /**
     * @param configuration the configuration to set
     */
    public void setConfiguration(ApplicationSettings configuration) {
        this.configuration = configuration;
    }

    public int getState() {
        return state;
    }

    @Override
    public void onOpen(InetSocketAddress inetSocketAddress) {
        state = WebSocketState.OPEN;
        Logger.getLogger(ApplicationSettings.class.getName()).log(Level.INFO, "Sever Opening Port : {0}", inetSocketAddress);
    }

    @Override
    public void onAccept(SocketChannel socketChannel) {
    }

    @Override
    public void onRead(SocketChannel socketChannel, byte[] data, int count) {
    }

    @Override
    public void onSend(SocketChannel socketChannel, byte[] data) {
    }

    @Override
    public void onWrite(SocketChannel socketChannel, int count) {
    }

    @Override
    public void onClose(SocketChannel socketChannel) {
        state = WebSocketState.CLOSED;
    }
}

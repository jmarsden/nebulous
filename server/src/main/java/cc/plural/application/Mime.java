/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.application;

import cc.plural.server.ServerException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import cc.plural.jsonij.JSON;
import cc.plural.jsonij.JSON.Object;
import cc.plural.jsonij.Value;
import cc.plural.jsonij.parser.ParserException;

/**
 * Mime Mapping Delegate. Loads the mime configuration for the server.
 *
 * @author jmarsden@plural.cc
 */
public class Mime {

    public static final String DEFAULT_MIME_RESOURCE = "/etc/mime.json";

    private Map<String, String> mimeTypes;

    protected Mime() {
        mimeTypes = new HashMap<String, String>();
    }

    public void registerMime(String extension, String mime) {
        if(mimeTypes.containsKey(extension)) {
            mimeTypes.remove(extension);
        }
        mimeTypes.put(extension, mime);
    }

    /**
     * Check if a mime type is defined for an extension.
     *
     * @param extension The extension to check.
     * @return true when a mime exists for the provided extension otherwise false.
     */
    public boolean hasMimeForExtension(String extension) {
        return mimeTypes.containsKey(extension);
    }

    /**
     * Retrieve the mime for an extension.
     *
     * @param extension The extension to retrieve.
     * @return The mime type for the provided extension. If no mime is defined for the extension then null.
     */
    public String getMimeForExtension(String extension) {
        return mimeTypes.get(extension);
    }

    /**
     * Static construction helper. This loads the mime types from the default location /etc/mime.json on the
     * resource path.
     *
     * @return A configured Mime object.
     * @throws ServerException Server Exception.
     */
    public static Mime loadMimeTypes() throws ServerException {
        return loadMimeTypes(DEFAULT_MIME_RESOURCE);
    }

    /**
     * Static construction helper. This loads the mime types from the a specified location. The mime resource
     * is a JSON object with extensions as properties and mime types as values.
     *
     * @param mimeResource The location of the mime resource.
     * @return A configured Mime object.
     * @throws ServerException Server Exception.
     */
    public static Mime loadMimeTypes(String mimeResource) throws ServerException {
        Mime mime = new Mime();
        URL resourceURL = mime.getClass().getResource(mimeResource);
        if(resourceURL != null) {
            InputStream inputStream = null;
            InputStreamReader inputStreamReader = null;
            try {
                inputStream = resourceURL.openStream();
                inputStreamReader = new InputStreamReader(inputStream);
                JSON mimeJSON = JSON.parse(inputStreamReader);
                if(mimeJSON.getRoot().type() == Value.TYPE.OBJECT) {
                    @SuppressWarnings("unchecked")
                    JSON.Object<String, Value> mimeJSONObject = (Object<String, Value>) mimeJSON.getRoot();
                    Iterator<String> keyIterator = mimeJSONObject.keySet().iterator();
                    while(keyIterator.hasNext()) {
                        String extension = keyIterator.next();
                        Value mimeType = mimeJSONObject.get(extension);
                        mime.registerMime(extension, mimeType.toString());
                    }
                }
            } catch(IOException e) {
                throw new ServerException("mimeLoad", mimeResource, e.toString());
            } catch(ParserException e) {
                throw new ServerException("mimeMalformed", mimeResource, e.toString());
            } finally {
                if(inputStreamReader != null) {
                    try {
                        inputStreamReader.close();
                    } catch(IOException e) {
                    }
                }
                if(inputStream != null) {
                    try {
                        inputStream.close();
                    } catch(IOException e) {
                    }
                }
            }
        }
        return mime;
    }

    public static String disambiguate(String fileContentType, String extensionContentType) {
        if(extensionContentType != null) {
            return extensionContentType;
        } else {
            return fileContentType;
        }
    }
}

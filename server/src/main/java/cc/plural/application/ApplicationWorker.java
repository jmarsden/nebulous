/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.application;

import cc.plural.server.ServerHandler;
import cc.plural.server.ServerException;
import cc.plural.nio.MessageData;
import cc.plural.nio.QueuedWorker;
import cc.plural.server.Server;
import java.net.Socket;
import java.nio.channels.SocketChannel;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ApplicationWorker extends QueuedWorker implements Runnable {

    private boolean running;

    private Map<Integer, ServerHandler> handlers;

    public ApplicationWorker() {
        handlers = new HashMap<Integer, ServerHandler>();
    }

    public void registerHandler(ServerHandler handler, int port) throws InstantiationException, IllegalAccessException {
        if(handlers.containsKey(port)) {
            // TODO: Throw an exception here.
        }
        handlers.put(port, handler);
    }

    public void run() {
        if(running) {
            return;
        } else {
            running = true;
        }
        MessageData<Server> messageData;
        while(running) {
            synchronized(workerQueue) {
                while(workerQueue.isEmpty()) {
                    try {
                        workerQueue.wait();
                    } catch(InterruptedException e) {
                    }
                }
                messageData = workerQueue.remove(0);
            }
            SocketChannel channel = messageData.getSocket();
            Socket socket = channel.socket();
            int localPort = socket.getLocalPort();
            // Find the handler for the given port.
            ServerHandler handler = handlers.get(localPort);
            try {
                handler.handleMessage(messageData);
            } catch(ServerException ex) {
                Logger.getLogger(ApplicationWorker.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}

/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.application.configuration;

public class HttpSettings {

    String handler;
    String[] index;
    String headers;
    String mime;
    String defaultMime;
    ServerSettings server[];

    public HttpSettings() {
	handler = null;
    }

    public String getHandler() {
	return handler;
    }

    public void setHandler(String handler) {
	this.handler = handler;
    }

    public String[] getIndex() {
	return index;
    }

    public void setIndex(String[] index) {
	this.index = index;
    }

    public String getHeaders() {
	return headers;
    }

    public void setHeaders(String headers) {
	this.headers = headers;
    }

    public String getDefaultMime() {
	return defaultMime;
    }

    public void setDefaultMime(String defaultMime) {
	this.defaultMime = defaultMime;
    }

    public String getMime() {
	return mime;
    }

    public void setMime(String mime) {
	this.mime = mime;
    }

    public ServerSettings[] getServer() {
	return server;
    }

    public void setServer(ServerSettings[] server) {
	this.server = server;
    }
}

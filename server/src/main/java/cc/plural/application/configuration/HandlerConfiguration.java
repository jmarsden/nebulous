/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cc.plural.application.configuration;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author John
 */
public class HandlerConfiguration {
    public Class<?> implemenetation;
    public Class<?> configuration;
    
    Map<String, RendererConfiguration> renderers;
    
    public HandlerConfiguration() {
        renderers = new HashMap<String, RendererConfiguration>();
    }

    public Class<?> getImplemenetation() {
        return implemenetation;
    }

    public void setImplemenetation(Class<?> implemenetation) {
        this.implemenetation = implemenetation;
    }

    public Class<?> getConfiguration() {
        return configuration;
    }

    public void setConfiguration(Class<?> configuration) {
        this.configuration = configuration;
    }

    public Map<String, RendererConfiguration> getRenderers() {
        return renderers;
    }

    public void setRenderers(Map<String, RendererConfiguration> renderers) {
        this.renderers = renderers;
    }
    
    
}

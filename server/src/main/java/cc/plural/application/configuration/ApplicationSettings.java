/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.application.configuration;

import cc.plural.jsonij.Value;
import cc.plural.jsonij.marshal.annotation.JSONCollector;
import java.util.HashMap;
import java.util.Map;

public class ApplicationSettings {

    @JSONCollector(true)
    public Map<String, Value> handlerConfigurationMap;

    public ApplicationSettings() {
	handlerConfigurationMap = new HashMap<String, Value>();
    }

    public Map<String, Value> getHandlerConfigurationMap() {
        return handlerConfigurationMap;
    }

    public void setHandlerConfigurationMap(Map<String, Value> handlerConfigurationMap) {
        this.handlerConfigurationMap = handlerConfigurationMap;
    }  
}

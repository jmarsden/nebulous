/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.application;

import cc.plural.application.configuration.ApplicationSettings;
import cc.plural.application.configuration.HandlerConfiguration;
import cc.plural.application.configuration.SystemConfiguration;
import cc.plural.jsonij.Value;
import cc.plural.jsonij.marshal.JSONMarshaler;
import cc.plural.jsonij.marshal.JSONMarshalerException;
import cc.plural.server.ServerException;
import cc.plural.server.ServerHandler;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServerMain {

    public List<ApplicationWorker> workers;
    public List<ApplicationServer> servers;
    public static final int MIN_PORT_NUMBER = 1000;
    public static final int MAX_PORT_NUMBER = 11000;

    public ServerMain() {
        workers = new ArrayList<ApplicationWorker>();
        servers = new ArrayList<ApplicationServer>();
    }

    public void runServer() {
        Logger.getLogger(ServerMain.class.getName()).log(Level.INFO, "Starting Server {0}", new Date());

        URL handlerURL = ClassLoader.class.getResource("/etc/system.json");
        SystemConfiguration handlerConfig = null;
        try {
            handlerConfig = (SystemConfiguration) JSONMarshaler.marshalJSON(handlerURL.openStream(), SystemConfiguration.class);
        } catch (IOException ex) {
            Logger.getLogger(ServerMain.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JSONMarshalerException ex) {
            Logger.getLogger(ServerMain.class.getName()).log(Level.SEVERE, null, ex);
        }

        ApplicationSettings applicationConfiguration = null;
        ServerHandler handlerInstance;
        URL url = ClassLoader.class.getResource("/etc/application.json");
        try {
            applicationConfiguration = (ApplicationSettings) JSONMarshaler.marshalJSON(url.openStream(), ApplicationSettings.class);
        } catch (IOException ex) {
            Logger.getLogger(ServerMain.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JSONMarshalerException ex) {
            Logger.getLogger(ServerMain.class.getName()).log(Level.SEVERE, null, ex);
        }

        Map<String, Value> configurationDataMap = applicationConfiguration.getHandlerConfigurationMap();
        Map<String, HandlerConfiguration> configurationMap = handlerConfig.getHandlers();

        for (String handler : configurationMap.keySet()) {
            handlerInstance = null;
            Value handlerSettingsData = configurationDataMap.get(handler);
            HandlerConfiguration handlerConfiguration = configurationMap.get(handler);
            Class<?> handlerSettings = handlerConfiguration.getConfiguration();
            Class<?> handlerImplementation = handlerConfiguration.getImplemenetation();
            try {
                Object handlerSettingsInstance = JSONMarshaler.marshalJSON(handlerSettingsData, handlerSettings);
                Constructor<?>[] handlerConstructors = handlerImplementation.getConstructors();
                for (Constructor<?> handlerConstructor : handlerConstructors) {
                    Class<?>[] handlerConstructorParmTypes = handlerConstructor.getParameterTypes();
                    if (handlerConstructorParmTypes.length == 1) {
                        Class<?> handlerConstructorParm = handlerConstructorParmTypes[0];
                        if (handlerConstructorParm.equals(handlerSettings)) {

                            try {
                                handlerInstance = (ServerHandler) handlerConstructor.newInstance(handlerSettingsInstance);
                                handlerInstance.configure();
                            } catch (ServerException ex) {
                                Logger.getLogger(ServerMain.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (InstantiationException ex) {
                                Logger.getLogger(ServerMain.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (IllegalAccessException ex) {
                                Logger.getLogger(ServerMain.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (IllegalArgumentException ex) {
                                Logger.getLogger(ServerMain.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (InvocationTargetException ex) {
                                Logger.getLogger(ServerMain.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    }

                }
            } catch (JSONMarshalerException ex) {
                Logger.getLogger(ServerMain.class.getName()).log(Level.SEVERE, null, ex);
            }

            int[] ports = null;
            if (handlerInstance.getPorts() == null) {
                int port = 10909;
                while (!available(port)) {
                    port++;
                    if (port > MAX_PORT_NUMBER) {
                        break;
                    }
                }
                ports = new int[]{port};
            } else {
                ports = handlerInstance.getPorts();
            }

            try {
                ApplicationWorker worker = new ApplicationWorker();
                workers.add(worker);
                for (int i = 0; i < ports.length; i++) {
                    ApplicationServer server = new ApplicationServer(null, ports[i], worker);
                    worker.registerHandler(handlerInstance, ports[i]);
                    servers.add(server);
                    new Thread(server).start();
                }
                new Thread(worker).start();
            } catch (IOException ex) {
                Logger.getLogger(ServerMain.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InstantiationException ex) {
                Logger.getLogger(ServerMain.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(ServerMain.class.getName()).log(Level.SEVERE, null, ex);
            }


            if (ports.length > 0) {
                BrowserUtils.openURL("http://localhost:" + ports[0]);
            }
        }
    }

    public static void main(String[] args) {
        ServerMain server = new ServerMain();
        server.runServer();
    }

    /*
     * Checks to see if a specific port is available.
     *
     * @param port the port to check for availability
     */
    public static boolean available(int port) {
        if (port < MIN_PORT_NUMBER || port > MAX_PORT_NUMBER) {
            throw new IllegalArgumentException("Invalid start port: " + port);
        }
        ServerSocket ss = null;
        DatagramSocket ds = null;
        try {
            ss = new ServerSocket(port);
            ss.setReuseAddress(true);
            ds = new DatagramSocket(port);
            ds.setReuseAddress(true);
            return true;
        } catch (IOException e) {
        } finally {
            if (ds != null) {
                ds.close();
            }

            if (ss != null) {
                try {
                    ss.close();
                } catch (IOException e) {
                    /*
                     * should not be thrown
                     */
                }
            }
        }
        return false;
    }
}

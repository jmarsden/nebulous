/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.template;

import java.io.IOException;
import java.net.URL;

import cc.plural.resource.ResourceUtils;

public class PageTemplate {

	protected String name;
	protected String templateLayout;
	
	protected PageTemplate(String name) {
		if(name == null) {
			throw new NullPointerException("Name cannot be null.");
		}
		this.name = name;
	}
	
	protected void loadTemplateLayout() throws IOException {
		URL resourceURL = getClass().getResource(String.format("/template/%s", name));
		if(resourceURL != null) {
			byte[] templateBytes = ResourceUtils.obtainByteData(resourceURL);
			templateLayout = new String(templateBytes);
		} else {
			// TODO: Fixme
			throw new NullPointerException("Fixme!");
		}
	}
	
	public String processPage(TemplateVariables variables) {
		return "Rah";
	}
	
	public static PageTemplate getPageTemplate(String name) throws IOException {
		PageTemplate template = new PageTemplate(name);
		template.loadTemplateLayout();
		return template;
	}
}

/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.websocket;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.channels.SocketChannel;

import cc.plural.nio.NIOServer;
import cc.plural.server.ServerWorker;

public class WebSocketServer extends NIOServer {
	
	private int state;
	
	public WebSocketServer(InetAddress hostAddress, int port, ServerWorker worker) throws IOException {
		super(hostAddress, port, worker);
		state = WebSocketState.CONNECTING;
	}

	public int getState() {
		return state;
	}

	public void onOpen(InetSocketAddress inetSocketAddress) {
		state = WebSocketState.OPEN;
		System.out.println("Open:" + inetSocketAddress);
	}
	
	public void onAccept(SocketChannel socketChannel) {
	}
	
	public void onRead(SocketChannel socketChannel, byte[] data, int count) {
	}
	
	public void onSend(SocketChannel socketChannel, byte[] data) {
	}
	
	public void onWrite(SocketChannel socketChannel, int count) {
	}
	
	public void onClose(SocketChannel socketChannel) {
		state = WebSocketState.CLOSED;
	}
}

/**
 * Copyright (C) 2012 J.W.Marsden <jmarsden@plural.cc>
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package cc.plural.websocket;

import java.io.IOException;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;

import cc.plural.server.ServerWorker;

public class WebSocket {
	
	private static Map<WebSocketServerKey, WebSocketServer> webSocketServers;
	
	static {
		webSocketServers = new HashMap<WebSocketServerKey, WebSocketServer>();
	}
	
	public WebSocket() {
		
	}

	public static WebSocketServer createWebSocketServer(int port) throws IOException  {
		return createWebSocketServer(null, port, null);
	}
	
	public static WebSocketServer createWebSocketServer(int port, ServerWorker worker) throws IOException  {
		return createWebSocketServer(null, port, worker);
	}
	
	public static WebSocketServer createWebSocketServer(InetAddress hostAddress, int port) throws IOException {
		return createWebSocketServer(hostAddress, port, null);
	}
	
	public static WebSocketServer createWebSocketServer(InetAddress hostAddress, int port, ServerWorker worker) throws IOException {
		WebSocketServerKey key = new WebSocketServerKey(hostAddress, port);
		if(webSocketServers.containsKey(key)) {
			//TODO: Make this a WebSocket Error.
			throw new RuntimeException("Address In Use");
		} else {
			WebSocketServer webSocketServer = new WebSocketServer(hostAddress, port, worker);
			webSocketServers.put(key, webSocketServer);
			return webSocketServer;
		}
	}

	public static final class WebSocketServerKey {
		private final InetAddress hostAddress;
		private final int port;
		
		public WebSocketServerKey(InetAddress hostAddress, int port) {
			this.hostAddress = hostAddress;
			this.port = port;
		}

		public InetAddress getHostAddress() {
			return hostAddress;
		}

		public int getPort() {
			return port;
		}

		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((hostAddress == null) ? 0 : hostAddress.hashCode());
			result = prime * result + port;
			return result;
		}

		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			WebSocketServerKey other = (WebSocketServerKey) obj;
			if (hostAddress == null) {
				if (other.hostAddress != null)
					return false;
			} else if (!hostAddress.equals(other.hostAddress))
				return false;
			if (port != other.port)
				return false;
			return true;
		}
	}
}
